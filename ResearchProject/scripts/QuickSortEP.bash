#!/bin/bash

#CLASS_PATH=src/*:lib/junit-4.9.jar:lib/pitest-0.31.jar
CLASS_PATH=../src:../lib/pitest-0.31.jar:../lib/junit-4.9.jar:../bin

MUTATORS=CONDITIONALS_BOUNDARY,NEGATE_CONDITIONALS,MATH,INCREMENTS,INVERT_NEGS,INLINE_CONSTS,RETURN_VALS

REPORT_DIR=report/

TARGET_CLASSES=swen90006.progs.QuickSort
TARGET_TESTS=swen90006.tests.QuickSortEP

SOURCE_DIRS=src/

EXCLUDED_METHODS=test

# the actual java commands
java -cp $CLASS_PATH \
    org.pitest.mutationtest.MutationCoverageReport \
    --reportDir $REPORT_DIR \
    --targetClasses $TARGET_CLASSES \
    --targetTests $TARGET_TESTS \
    --sourceDirs $SOURCE_DIRS \
    --mutators $MUTATORS \
    --excludedMethods $EXCLUDED_METHODS
