package swen90006.framework;

public interface TestOracle<I, R> {
	public abstract boolean verify(I input, R result);
}
