package swen90006.framework;

import junit.framework.TestCase;

import swen90006.framework.TestSuite.TestsType;
import swen90006.oracles.SearchOracle;
import swen90006.oracles.SortOracle;
import swen90006.progs.BinarySearch;
import swen90006.progs.MergeSort;
import swen90006.progs.QuickSort;
import swen90006.testsuites.MergeSortTestSuite;
import swen90006.testsuites.QuickSortTestSuite;
import swen90006.testsuites.SearchTestSuite;

public abstract class TestDriver extends TestCase {
	/**
	 * Basically the program that is to be tested, along with its oracle and test suite.
	 */
	public static class Package<InputT, ReturnT> {
		public Testable<InputT, ReturnT> _program;
		public TestOracle<InputT, ReturnT> _oracle;
		public TestSuite<InputT> _suite;

		public Package(Testable<InputT, ReturnT> program, TestOracle<InputT, ReturnT> oracle, TestSuite<InputT> suite) {
			_program = program;
			_oracle = oracle;
			_suite = suite;
		}
	}
	
	protected static final Package<SearchQuery, Integer> BINARY_SEARCH =
			new Package<SearchQuery, Integer>(new BinarySearch(), new SearchOracle(), new SearchTestSuite());
	protected static final Package<int[], int[]> MERGE_SORT =
			new Package<int[], int[]>(new MergeSort(), new SortOracle(), new MergeSortTestSuite());
	protected static final Package<int[], int[]> QUICK_SORT =
			new Package<int[], int[]>(new QuickSort(), new SortOracle(), new QuickSortTestSuite());

	/**
	 * Perform the given types of tests on the given package.
	 */
	protected <InputT, ReturnT> void simpleTest(Package<InputT, ReturnT> pkg, TestsType type) {
		for(InputT input : pkg._suite.getTests(type)) {
			assertTrue(pkg._oracle.verify(input, pkg._program.test(input)));
		}
	}
}
