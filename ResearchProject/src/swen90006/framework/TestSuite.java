package swen90006.framework;

import java.util.ArrayList;
import java.util.List;

public abstract class TestSuite<I> {
	
	public enum TestsType { BVA, BRANCH, EP, ALL };

	protected abstract void generateEquivalencePartitioningTests(List<I> tests);
	protected abstract void generateBoundaryValueTests(List<I> tests);
	protected abstract void generateBranchCoverageTests(List<I> tests);

	public List<I> getTests(TestsType type) {
		List<I> tests;

		// create a new list to put all the tests in
		tests = new ArrayList<I>();

		// generate all the tests
		if(type == TestsType.BRANCH || type == TestsType.ALL) {
			generateBranchCoverageTests(tests);
		}
		if(type == TestsType.BVA || type == TestsType.ALL) {
			generateBoundaryValueTests(tests);
		}
		if(type == TestsType.EP || type == TestsType.ALL) {
			generateEquivalencePartitioningTests(tests);
		}

		// return these tests
		return tests;
	}
	
	public static int[] orderedArray(int length) {
		int[] array = new int[length];
		
		for(int i = 0; i < length; i++){
			array[i] = i;
		}
		
		return array;
	}
	
	public static int[] reverseOrderArray(int length){
		
		int[] array = new int[length];
		
		for(int i = length - 1; i >= 0; i--){
			array[i] = i;
		}
		
		return array;
	}
}
