package swen90006.framework;

public class SearchQuery {
	public int[] array;
	public int key;
	
	public SearchQuery(int[] array, int key){
		this.array = array;
		this.key = key;
	}
}
