package swen90006.framework;

/**
 * 
 * @author pmansour
 *
 * @param <I> The type of input this takes.
 * @param <R> The type of result it outputs.
 */
public interface Testable<I, R> {

	public abstract R test(I input);
}
