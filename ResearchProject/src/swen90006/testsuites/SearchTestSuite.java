package swen90006.testsuites;

import java.util.List;

import swen90006.framework.SearchQuery;
import swen90006.framework.TestSuite;

public class SearchTestSuite extends TestSuite<SearchQuery> {

	@Override
	protected void generateEquivalencePartitioningTests(List<SearchQuery> tests) {
		// null array (any key)
		tests.add(new SearchQuery(null, 0));
		// empty array, any key
		tests.add(new SearchQuery(new int[0], 0));
		// len(array) 1, contains negative, key is negative, occurrences = 0
		tests.add(new SearchQuery(new int[] { -1 }, -2));
		// len(array) 1, contains negative, key is negative, occurrences = 1
		tests.add(new SearchQuery(new int[] { -1 }, -1));
		// len(array) 1, contains negative, key is positive, occurrences = 0
		tests.add(new SearchQuery(new int[] { -2 }, 3));
		// len(array) 1, doesn't contain negative, key is negative, occurrences = 0
		tests.add(new SearchQuery(new int[] { 1 }, -2));
		// len(array) 1, doesn't contain negative, key is positive, occurrences = 0
		tests.add(new SearchQuery(new int[] { 1 }, 2));
		// len(array) 1, doesn't contain negative, key is positive, occurrences = 1
		tests.add(new SearchQuery(new int[] { 1 }, 1));
		// len(array) *, contains negative, key is negative, occurrences = 0
		tests.add(new SearchQuery(new int[] { -1, 3, 6, 7 }, -2));
		// len(array) *, contains negative, key is negative, occurrences = 1
		tests.add(new SearchQuery(new int[] { -1, 3, 6, 7 }, -1));
		// len(array) *, contains negative, key is negative, occurrences = *
		tests.add(new SearchQuery(new int[] { -1, -1, 3, 6, 7 }, -1));
		// len(array) *, contains negative, key is positive, occurrences = 0
		tests.add(new SearchQuery(new int[] { -1, 3, 6, 7 }, 2));
		// len(array) *, contains negative, key is positive, occurrences = 1
		tests.add(new SearchQuery(new int[] { -1, 3, 6, 7 }, 3));
		// len(array) *, contains negative, key is positive, occurrences = *
		tests.add(new SearchQuery(new int[] { -1, 3, 6, 7, 7 }, 7));
		// len(array) *, doesn't contain negative, key is negative, occurrences = 0
		tests.add(new SearchQuery(new int[] { 2, 3, 6, 7 }, -1));
		// len(array) *, doesn't contain negative, key is positive, occurrences = 0
		tests.add(new SearchQuery(new int[] { 2, 3, 6, 7 }, 1));
		// len(array) *, doesn't contain negative, key is positive, occurrences = 1
		tests.add(new SearchQuery(new int[] { 2, 3, 6, 7 }, 3));
		// len(array) *, doesn't contain negative, key is positive, occurrences = *
		tests.add(new SearchQuery(new int[] { 2, 3, 6, 7, 7 }, 7));
		
	}

	@Override
	protected void generateBoundaryValueTests(List<SearchQuery> tests) {
		tests.add(new SearchQuery(new int[] {0,2}, 1));
		tests.add(new SearchQuery(new int[] {0,1,2}, 1));
		tests.add(new SearchQuery(new int[] {0,1,2}, 0));
		tests.add(new SearchQuery(TestSuite.orderedArray(100001), 100000));
		tests.add(new SearchQuery(TestSuite.orderedArray(100001), 50001));
	}

	@Override
	protected void generateBranchCoverageTests(List<SearchQuery> tests) {
		tests.add(new SearchQuery(new int[] {1,2,3}, 1));
		tests.add(new SearchQuery(new int[] {1,2,3}, 0));
		tests.add(new SearchQuery(new int[] {1,2,3}, 3));		
	}
}
