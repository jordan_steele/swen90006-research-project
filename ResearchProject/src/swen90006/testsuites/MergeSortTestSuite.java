package swen90006.testsuites;

import java.util.List;

import swen90006.framework.TestSuite;

public class MergeSortTestSuite extends TestSuite<int[]> {

	@Override
	protected void generateEquivalencePartitioningTests(List<int[]> tests) {
		// a null array
		tests.add(null);
		// an array of 0 elements
		tests.add(new int[0]);
		// sorted array of size 1 with negative element
		tests.add(new int[] { -1 });
		// sorted array of size 1 with no negative elements
		tests.add(new int[] { 1 });
		// unsorted array of size many with negative element(s)
		tests.add(new int[] { -3, -5, 2 });
		// unsorted array of size many without negative elements
		tests.add(new int[] { 2, 4, 8 });
		// sorted array of size many with negative element(s)
		tests.add(new int[] { -1, 3, 6, 7 });
		// sorted array of size many without negative element(s)
		tests.add(new int[] { 2, 5, 7 });
	}

	@Override
	protected void generateBoundaryValueTests(List<int[]> tests) {
		tests.add(new int[] {0});
		tests.add(new int[] {0,1});
		tests.add(new int[] {10,9,8,7,6,5,4,3,2,1});
		tests.add(TestSuite.orderedArray(100001));
		tests.add(TestSuite.reverseOrderArray(100000));
	}

	@Override
	protected void generateBranchCoverageTests(List<int[]> tests) {
		tests.add(new int[] {0});
		tests.add(new int[] {0, 1});
		tests.add(new int[] {1,0});
	}

}
