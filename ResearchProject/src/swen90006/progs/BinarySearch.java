package swen90006.progs;

import swen90006.framework.SearchQuery;
import swen90006.framework.Testable;

/** This BinarySearch implementation requires that the input array has a length
 *  between 1 and 100.*/

public class BinarySearch implements Testable<SearchQuery, Integer> {
	
	private int[] _array;
	
	@Override
	public Integer test(SearchQuery input) {
	    // Check for null value or value outside the required range
	    if (input.array == null || input.array.length == 0){
	      return -2;
	    }
		
		this._array = input.array;
		return binarySearch(input.key);
	}
	
	private int binarySearch(int key) {
	    int low = 0;
	    int high = _array.length -1;
	    int mid;
	    while (low <= high) {
	        mid = (low + high) / 2;
	        if (_array[mid] > key) {
	            high = mid - 1;
	        } else if (_array[mid] < key) {
	            low = mid + 1;
	        } else {
	            return mid;
	        }
	    }
	    return -1;
	}
}
