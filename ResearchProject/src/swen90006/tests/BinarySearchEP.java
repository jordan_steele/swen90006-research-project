package swen90006.tests;

import org.junit.Test;

import swen90006.framework.TestDriver;
import swen90006.framework.TestSuite.TestsType;

public class BinarySearchEP extends TestDriver {
	
	@Test
	public void test() {
		// test binary search with EP
		simpleTest(BINARY_SEARCH, TestsType.EP);
	}

}
