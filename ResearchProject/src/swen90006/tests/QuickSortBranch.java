package swen90006.tests;

import org.junit.Test;

import swen90006.framework.TestDriver;
import swen90006.framework.TestSuite.TestsType;

public class QuickSortBranch extends TestDriver {

	@Test
	public void test() {
		simpleTest(QUICK_SORT, TestsType.BRANCH);
	}
}
