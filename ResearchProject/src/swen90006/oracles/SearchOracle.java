package swen90006.oracles;

import swen90006.framework.SearchQuery;
import swen90006.framework.TestOracle;

public class SearchOracle implements TestOracle<SearchQuery, Integer> {
	
	@Override
	public boolean verify(SearchQuery input, Integer result) {
		//if it says that the array is not of the right size
		if (result == -2){
			//Check if it is actually outside our range
			if (input.array == null || input.array.length < 1){
				return true;
			}
			return false;
		}	
		
		// if it says it wasn't found
		if(result == -1) {
			// make sure it's not actually there
			for(int i = 0; i < input.array.length; i++) {
				// if it is,
				if(input.array[i] == input.key) {
					// then this is wrong
					return false;
				}
			}
			// otherwise, it's not really there, so it's correct
			return true;
		// otherwise,
		} else {
			// make sure it exists at the specified index
			return input.array[result] == input.key;
		}
	}
	
}
