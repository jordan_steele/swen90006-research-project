package swen90006.oracles;

import swen90006.framework.TestOracle;

public class SortOracle implements TestOracle<int[], int[]>{

	@Override
	public boolean verify(int[] input, int[] result) {
		int prev;
		
		//if program returned an out of bounds error
		if (result == null){
			//check if the input really was out of bounds
			if (input == null || input.length <= 0){
				return true;
			}
			return false;
		}
		
		// start with the first element
		prev = result[0];
		
		// for each upcoming element
		for(int i = 1; i < result.length; i++){
			// if its smaller than the last one,
			if (result[i] < prev){
				// this is incorrect order
				return false;
			}
		}
		
		// if it got this far, then its sorted
		return true;
	}
	
}
