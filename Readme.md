***Running Mutation Tests:
The scripts for running mutation tests are contained within the 'scripts/' folder. The script 'allTests.bash' will run the three programs with the three different test suites for each one. The other nine scripts correspond to the nine individual tests. Output is displayed on the command line and also as html files in a 'report/' folder.


